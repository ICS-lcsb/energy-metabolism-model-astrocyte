This repository contains the code to replicate the results presented in the paper titled "Mechanistic multiscale modelling of energy metabolism in human astrocytes indicates morphological effects in Alzheimer's Disease".
[ Reference Farina et al. DOI: https://doi.org/10.1101/2022.07.21.500921 ]

The three folder contains the code to replicate the three section of results of the manuscript.

To run Experiment 1 and Experiment 2, it is required to have Python 3.9.2 and Fenics 2019.1.0 (2019).

To run Experiment 3, it is required Python 2.7.6, Fenics 1.5 (2005) and the CutFEM library [Burman et al. 2015].
The astrocytic images, ready to be used in Experiment 3, for the control and reactive are included in the respective folders as numpy array.

For more information please contact the first author: Sofia Farina or the corresponding author: Alexander Skupin
